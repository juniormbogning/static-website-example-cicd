FROM nginx:1.21.1

LABEL maintainer="Junior Mbogning"

RUN apt-get update &&  \
    apt-get install -y curl && \
    apt-get install -y git
RUN rm -Rf /usr/share/nginx/html/*

RUN git clone https://github.com/diranetafen/static-website-example.git /usr/share/nginx/html

EXPOSE 80

#ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
CMD ["nginx", "-g", "daemon off;"]
